import numpy as np
import pandas as pd


class Watkins:
    def __init__(self, actions, learning_rate=0.01, reward_decay=0.9, e_greedy=0.1, lambda_var=0.9):
        self.actions = actions
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.lambda_var = lambda_var
        self.q_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.e_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.display_name="Watkins"

    def choose_action(self, state):
        self.check_state_exist(state)
        self.check_e_exists(state)
        action_probs = [self.epsilon/len(self.actions)]*len(self.actions)
        best_value = np.amax(self.q_table.loc[str(state)])
        best_actions = [i for i, x in enumerate(self.q_table.loc[str(state)]) if x == best_value]
        for best_action in best_actions:
            action_probs[best_action] += (1-self.epsilon) / len(best_actions)
        return np.random.choice(self.actions, p=action_probs)

    def get_greedy_action(self, state, a_):
        best_value = np.amax(self.q_table.loc[str(state)])
        best_actions = [i for i, x in enumerate(self.q_table.loc[str(state)]) if x == best_value]
        if a_ in best_actions:
            return a_
        else:
            return best_actions[0]

    def learn(self, s, a, r, s_):
        self.check_state_exist(s_)
        self.check_e_exists(s_)
        a_ = self.choose_action(str(s_))
        a_star = self.get_greedy_action(s_, a_)

        delta = r + self.gamma*self.q_table.loc[str(s_)][a_star] - self.q_table.loc[str(s)][a]
        # delta = r + self.gamma*self.q_table.loc[str(s_)][a_] - self.q_table.loc[str(s)][a]
        self.e_table.loc[str(s)][a] += 1

        self.q_table += self.e_table * self.lr * delta
        if a_ == a_star:
            self.e_table *= self.gamma * self.lambda_var
        else:
            self.clear_eligibility_traces()

        return s_, a_

    '''States are dynamically added to the Q(S,A) table as they are encountered'''
    def check_state_exist(self, state):
        if state not in self.q_table.index:
            # append new state to q table
            self.q_table = self.q_table.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.q_table.columns,
                    name=state,
                )
            )

    def check_e_exists(self, state):
        if state not in self.e_table.index:
            # append new state to e table
            self.e_table = self.e_table.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.e_table.columns,
                    name=state,
                )
            )
    
    def clear_eligibility_traces(self):
        for col in self.e_table.columns:
            self.e_table[col].values[:] = 0




class SARSALambda:
    def __init__(self, actions, learning_rate=0.01, reward_decay=0.9, e_greedy=0.1, lambda_var=0.9):
        self.actions = actions
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.lambda_var = lambda_var
        self.q_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.e_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.display_name="SARSALambda"

    def choose_action(self, state):
        self.check_state_exist(state)
        self.check_e_exists(state)
        action_probs = [self.epsilon/len(self.actions)]*len(self.actions)
        best_value = np.amax(self.q_table.loc[str(state)])
        best_actions = [i for i, x in enumerate(self.q_table.loc[str(state)]) if x == best_value]
        for best_action in best_actions:
            action_probs[best_action] += (1-self.epsilon) / len(best_actions)
        return np.random.choice(self.actions, p=action_probs)


    def learn(self, s, a, r, s_):
        self.check_state_exist(s_)
        self.check_e_exists(s_)
        a_ = self.choose_action(str(s_))

        delta = r + self.gamma*self.q_table.loc[str(s_)][a_] - self.q_table.loc[str(s)][a]
        self.e_table.loc[str(s)][a] += 1

        self.q_table += self.e_table * self.lr * delta
        self.e_table *= self.gamma * self.lambda_var

        return s_, a_

    '''States are dynamically added to the Q(S,A) table as they are encountered'''
    def check_state_exist(self, state):
        if state not in self.q_table.index:
            # append new state to q table
            self.q_table = self.q_table.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.q_table.columns,
                    name=state,
                )
            )

    def check_e_exists(self, state):
        if state not in self.e_table.index:
            # append new state to e table
            self.e_table = self.e_table.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.e_table.columns,
                    name=state,
                )
            )
    
    def clear_eligibility_traces(self):
        for col in self.e_table.columns:
            self.e_table[col].values[:] = 0


