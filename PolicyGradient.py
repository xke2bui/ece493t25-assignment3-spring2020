import numpy as np
import pandas as pd


class PolicyGradient:
    def __init__(self, actions, learning_rate=0.01, reward_decay=0.99):
        self.actions = actions
        self.lr = learning_rate
        self.gamma = reward_decay
        self.q_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.e_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.display_name="PolicyGradient"
        self.theta = np.zeros((10, 10, 4)).flatten()

    def choose_action(self, state):
        return np.random.choice(self.actions, p=self.get_action_probs(state))
    
    def get_action_probs(self, state):
        feature_vectors = self.get_feature_vectors(state)
        h_values = [np.exp(np.dot(self.theta, i)) for i in feature_vectors]
        sum_h = sum(h_values)
        if sum_h > 0:
            action_probs = [i/sum_h for i in h_values]
            return action_probs
        else:
            return [0.25, 0.25, 0.25, 0.25]

    def get_feature_vectors(self, state):
        new_state = [int((i - 5) / 40) for i in state[0:2]]
        feature_vectors = [np.zeros((10, 10, 4)) for i in range(4)]
        for i in self.actions:
            feature_vectors[i][new_state[0], new_state[1], i] += 1
            feature_vectors[i] = feature_vectors[i].flatten()
        return feature_vectors

    def get_feature_vector(self, state, action):
        new_state = [int((i - 5) / 40) for i in state[0:2]]
        feature_vector = np.zeros((10, 10, 4))
        feature_vector[new_state[0], new_state[1], action] += 1
        return feature_vector.flatten()

    def learn(self, s, a, r, s_):
        a_ = self.choose_action(s_)
        return s_, a_

    def reinforce(self, T, rewards, states, actions):
        for t in range(T):
            G_t = 0
            for k in range(t+1, T+1):
                G_t += rewards[k] * (self.gamma ** (k-t-1))
            current_feature_vector = self.get_feature_vector(states[t], actions[t])
            action_probs = self.get_action_probs(states[t])
            feature_vectors = self.get_feature_vectors(states[t])
            feature_expectation = np.zeros((10, 10, 4)).flatten()
            for i in range(len(self.actions)):
                feature_expectation += action_probs[i] * feature_vectors[i]
            gradient = current_feature_vector - feature_expectation
            self.theta += self.lr * (self.gamma**t) * G_t * gradient

