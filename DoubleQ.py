import numpy as np
import pandas as pd


class DoubleQ:
    def __init__(self, actions, learning_rate=0.05, reward_decay=0.9, e_greedy=0.1):
        self.actions = actions
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.q_table_1 = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.q_table_2 = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.display_name="Double Q Learning"
    
    def choose_action(self, state):
        self.check_state_exist_1(state)
        self.check_state_exist_2(state)
        action_probs = [self.epsilon/len(self.actions)]*len(self.actions)
        summed_row = self.q_table_1.loc[str(state)].add(self.q_table_2.loc[str(state)])
        best_value = np.amax(summed_row)
        best_actions = [i for i, x in enumerate(summed_row) if x == best_value]
        for best_action in best_actions:
            action_probs[best_action] += (1-self.epsilon) / len(best_actions)
        return np.random.choice(self.actions, p=action_probs)

    def learn(self, s, a, r, s_):
        self.check_state_exist_1(s_)
        self.check_state_exist_2(s_)
        a_ = self.choose_action(str(s_))
        # best_q = np.amax(self.q_table.loc[str(s_)])
        # self.q_table.loc[str(s)][a] += self.lr*(r + self.gamma*best_q - self.q_table.loc[str(s)][a])
        if np.random.random() < 0.5:
            best_value = np.amax(self.q_table_1.loc[str(s_)])
            best_actions = [i for i, x in enumerate(self.q_table_1.loc[str(s_)]) if x == best_value]
            best_action = np.random.choice(best_actions)
            self.q_table_1.loc[str(s)][a] += self.lr*(r + self.gamma*self.q_table_2.loc[str(s_)][best_action] - self.q_table_1.loc[str(s)][a])
        else:
            best_value = np.amax(self.q_table_2.loc[str(s_)])
            best_actions = [i for i, x in enumerate(self.q_table_2.loc[str(s_)]) if x == best_value]
            best_action = np.random.choice(best_actions)
            self.q_table_2.loc[str(s)][a] += self.lr*(r + self.gamma*self.q_table_1.loc[str(s_)][best_action] - self.q_table_2.loc[str(s)][a])
        return s_, a_

    '''States are dynamically added to the Q(S,A) table as they are encountered'''
    def check_state_exist_1(self, state):
        if state not in self.q_table_1.index:
            # append new state to q table
            self.q_table_1 = self.q_table_1.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.q_table_1.columns,
                    name=state,
                )
            )

    def check_state_exist_2(self, state):
        if state not in self.q_table_2.index:
            # append new state to q table
            self.q_table_2 = self.q_table_2.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.q_table_2.columns,
                    name=state,
                )
            )
