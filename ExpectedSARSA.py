import numpy as np
import pandas as pd


class ExpectedSARSA:
    def __init__(self, actions, learning_rate=0.05, reward_decay=0.9, e_greedy=0.1):
        self.actions = actions
        self.lr = learning_rate
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.q_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.display_name="ExpectedSARSA"

    def choose_action(self, state):
        self.check_state_exist(state)
        action_probs = [self.epsilon/len(self.actions)]*len(self.actions)
        best_value = np.amax(self.q_table.loc[str(state)])
        best_actions = [i for i, x in enumerate(self.q_table.loc[str(state)]) if x == best_value]
        for best_action in best_actions:
            action_probs[best_action] += (1-self.epsilon) / len(best_actions)
        return np.random.choice(self.actions, p=action_probs)

    def learn(self, s, a, r, s_):
        self.check_state_exist(s_)
        a_ = self.choose_action(str(s_))
        action_probs = [self.epsilon/len(self.actions)]*len(self.actions)
        best_value = np.amax(self.q_table.loc[str(s_)])
        best_actions = [i for i, x in enumerate(self.q_table.loc[str(s_)]) if x == best_value]
        for best_action in best_actions:
            action_probs[best_action] += (1-self.epsilon) / len(best_actions)
        expectation = 0
        for action_prob, action_value in zip(action_probs, self.q_table.loc[str(s_)]):
            expectation += action_prob * action_value
        self.q_table.loc[str(s)][a] += self.lr*(r + self.gamma*expectation - self.q_table.loc[str(s)][a])
        return s_, a_

    '''States are dynamically added to the Q(S,A) table as they are encountered'''
    def check_state_exist(self, state):
        if state not in self.q_table.index:
            # append new state to q table
            self.q_table = self.q_table.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.q_table.columns,
                    name=state,
                )
            )
